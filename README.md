# OpenFruit Database

This project aims to provide a crowdsourcing platform for data on publicly available fruit (and vegetable) plants.

Detailed documentation here: https://drive.google.com/drive/folders/1RV8XfusyZ5_Nb3VbLNyCv-GnK7ZKeIeE